// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "cs378_l2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_L2_API Acs378_l2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
